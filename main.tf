data "google_organization" "org" {
  domain = var.DOMAIN
}

data "google_billing_account" "acct" {
  display_name = var.BILLING_NAME_GENERAL
}

data "google_project" "vault_project" {
  project_id = length(var.VAULT_PROJECT_ID) == 0 ? google_project.vault_project[0].id : var.VAULT_PROJECT_ID
}

resource "random_string" "vault_project_id_suffix" {
  length = 21
  upper = false
  special = false
}

resource "google_project" "vault_project" {
  count = length(var.VAULT_PROJECT_ID) == 0 ? 1 : 0
  name = var.VAULT_PROJECT_NAME
  project_id = "is-vault-${random_string.vault_project_id_suffix.result}"
  org_id  = data.google_organization.org.org_id
  billing_account = data.google_billing_account.acct.id

  # Wait a little extra time because it seems like the billing takes awhile
  # to actually be setup
  provisioner "local-exec" {
    command = "sleep 30"
  }
}

module "vault_on_gcloud_1" {
  source = "./modules/core-vault-on-gcloud"

  VAULT_PROJECT_ID = length(var.VAULT_PROJECT_ID) == 0 ? google_project.vault_project[0].id : var.VAULT_PROJECT_ID
  VAULT_PROJECT_NUMBER = google_project.vault_project[0].number
  GCP_ACCESS_TOKEN = var.GCP_ACCESS_TOKEN
  ORG_ID = data.google_organization.org.org_id
  DOMAIN = var.DOMAIN
  VAULT_DOMAIN = var.VAULT_DOMAIN
}

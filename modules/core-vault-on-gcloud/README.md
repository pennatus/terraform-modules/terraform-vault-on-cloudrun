# Vault on Cloud Run Terraform Module

Use this module to build a fully operational Vault on GCP Cloud Run.

## Usage

```hcl
module "vault-on-cloud" {
  source = "git::https://gitlab.com/pennatus/infrastructure//modules/vault-on-gcloud"
  ADMIN_PROJECT = var.ADMIN_PROJECT
  GCP_ACCESS_TOKEN = var.GCP_ACCESS_TOKEN
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| ADMIN\_PROJECT | This is the infrastructure project to deploy vault under - project id | string | n/a | yes |
| DOMAIN | Domain of the organization | string | n/a | yes |
| GCP\_ACCESS\_TOKEN | Short lived GCP access token.  Get using `gcloud auth print-access-token` | string | n/a | yes |
| SQL\_INSTANCES | List of SQL instances to add connections to | list | `[]` | no |
| VAULT\_VERSION | This is the docker tagged version of Vault | string | `"latest"` | no |

## Outputs

| Name | Description |
|------|-------------|
| url |  |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

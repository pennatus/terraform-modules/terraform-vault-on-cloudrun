# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY A VAULT INSTANCE IN GOOGLE CLOUD RUN
# ---------------------------------------------------------------------------------------------------------------------

# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY THE VAULT INSTANCE
# ---------------------------------------------------------------------------------------------------------------------

resource "random_uuid" "keyring_uuid" { }
resource "random_uuid" "storage_uuid" { }

data "external" "sql_instances" {
  program = ["bash", "${path.module}/scripts/get_sql_instances.sh"]

  query = {    
    GCP_ACCESS_TOKEN = var.GCP_ACCESS_TOKEN
  }
}

# Used for making connections to databases
resource "google_project_service" "sqladminapi" {
  project = var.VAULT_PROJECT_ID
  service = "sqladmin.googleapis.com"
}

resource "google_project_service" "cloudresourcemanagerapi" {
  project = var.VAULT_PROJECT_ID
  service = "cloudresourcemanager.googleapis.com"
}

// This is necessary to refresh API states and to enable APIs
resource "google_project_service" "serviceusageapi" {
  project = var.VAULT_PROJECT_ID
  service = "serviceusage.googleapis.com"
}

// This is necessary to bind the org-owner security account at the org level.
resource "google_project_service" "iamapi" {
  project = var.VAULT_PROJECT_ID
  service = "iam.googleapis.com"
}

// Require cloud billing because once the Vault token is being used to grab a provisioner
// GCP service account it will be running from inside a project.
resource "google_project_service" "cloudbillingapi" {
  project = var.VAULT_PROJECT_ID
  service = "cloudbilling.googleapis.com"
}

resource "google_project_service" "runapi" {
  # This is a bit ugly but it seems like if this is the first time enabling runapi then
  # it can take some time for the permissions to propogate through the network.  I haven't
  # found a better way to do this.

  project = var.VAULT_PROJECT_ID
  service = "run.googleapis.com"
  disable_on_destroy = false
  depends_on = [ google_project_service.cloudbillingapi ]

  provisioner "local-exec" {
    command = "sleep 60"
  }
}

resource "google_project_service" "kmsapi" {
  project = var.VAULT_PROJECT_ID
  service = "cloudkms.googleapis.com"
  depends_on = [ google_project_service.cloudbillingapi ]
}

resource "google_storage_bucket" "vault_backend_storage" {
  name     = "vault-storage-${random_uuid.storage_uuid.result}"
  project = var.VAULT_PROJECT_ID
  location = "us-central1"
  force_destroy = "true"
}

resource "google_kms_key_ring" "vault_key_ring" {
  name = "vault-keyring-${random_uuid.keyring_uuid.result}"
  project = var.VAULT_PROJECT_ID
  location = "global"
  depends_on = [ google_project_service.kmsapi ]
}

resource "google_kms_crypto_key" "vault_key" {
  name            = "vault-key"
  key_ring        = google_kms_key_ring.vault_key_ring.id
  rotation_period = "15552000s"

  lifecycle {
    prevent_destroy = false
  }
}

locals {
  project_roles = [ "roles/storage.admin", "roles/cloudkms.cryptoKeyEncrypterDecrypter", "roles/cloudkms.admin" ]
  organization_roles = [ "roles/iam.serviceAccountAdmin", "roles/iam.serviceAccountKeyAdmin", "roles/resourcemanager.projectIamAdmin", "roles/resourcemanager.organizationAdmin", "roles/cloudsql.client" ]
}

resource "google_project_iam_member" "project_member_roles" {
  count = length(local.project_roles)
  project = var.VAULT_PROJECT_ID
  role    = local.project_roles[count.index]

  member = "serviceAccount:${var.VAULT_PROJECT_NUMBER}-compute@developer.gserviceaccount.com"

  depends_on = [ google_project_service.runapi ]
}

resource "google_organization_iam_member" "organization_member_roles" {
  count = length(local.organization_roles)
  org_id  = var.ORG_ID
  role    = local.organization_roles[count.index]

  member = "serviceAccount:${var.VAULT_PROJECT_NUMBER}-compute@developer.gserviceaccount.com"

  depends_on = [ google_project_service.runapi ]
}


resource "google_cloud_run_service" "vault" {
  name     = var.VAULT_SERVICE_NAME
  provider = google-beta
  location = "us-central1"

  metadata {
    namespace = var.VAULT_PROJECT_ID
  }

  project = var.VAULT_PROJECT_ID

  template {
    spec {
      containers {
        image = "gcr.io/ioka-docker-images/vault-on-docker:${var.VAULT_VERSION}"
        resources {
          limits = {
            memory = "512M"
          }
        }
        env {
          name = "VAULT_LOCAL_CONFIG"
          value = "{\"ui\": \"true\", \"plugin_directory\": \"/etc/vault/vault_plugins\", \"listener\": [{\"tcp\": {\"address\": \"0.0.0.0:7200\", \"tls_disable\": 1}}], \"backend\": {\"gcs\": {\"bucket\": \"vault-storage-${random_uuid.storage_uuid.result}\"}}, \"seal\": {\"gcpckms\": {\"region\": \"global\", \"project\": \"${var.VAULT_PROJECT_ID}\", \"key_ring\": \"vault-keyring-${random_uuid.keyring_uuid.result}\", \"crypto_key\": \"vault-key\"}"
        }
        env {
          name = "VAULT_ADDR"
          value = "http://127.0.0.1:7200"
        }
      }
    }

    metadata {
      annotations = {
        "run.googleapis.com/cloudsql-instances" = data.external.sql_instances.result.instances
      }
    }
  }

  # Use to set the IAM bindings to allow public access since the provider doesn't give
  # us another way yet.
  provisioner "local-exec" {
    command = "curl -X POST -d '{\"policy\": {\"bindings\":[{\"role\": \"roles/run.invoker\",\"members\": [\"allUsers\"]}]}}' -H \"Content-Type: application/json\" -H \"Authorization: Bearer ${var.GCP_ACCESS_TOKEN}\" https://us-central1-run.googleapis.com/v1/projects/${var.VAULT_PROJECT_ID}/locations/us-central1/services/${var.VAULT_SERVICE_NAME}:setIamPolicy"
  }

  depends_on = [google_project_iam_member.project_member_roles, google_organization_iam_member.organization_member_roles, google_project_service.runapi, google_storage_bucket.vault_backend_storage, google_kms_crypto_key.vault_key]
}

resource "null_resource" "wait_for_service" {
  provisioner "local-exec" {
    command = "sleep 60"
  }

  depends_on = [ google_cloud_run_service.vault ]
}

data "http" "service_status" {
  url = "https://us-central1-run.googleapis.com/v1/projects/${var.VAULT_PROJECT_ID}/locations/us-central1/services/${var.VAULT_SERVICE_NAME}"

  # Optional request headers
  request_headers = {
    Authorization = "Bearer ${var.GCP_ACCESS_TOKEN}"
    Accept = "application/json"
  }

  depends_on = [ null_resource.wait_for_service ]
}

resource "google_cloud_run_domain_mapping" "vault" {
  location = "us-central1"
  project = var.VAULT_PROJECT_ID
  provider = google-beta
  name     = var.VAULT_DOMAIN

  metadata {
    namespace = var.VAULT_PROJECT_ID
  }

  spec {
    route_name = var.VAULT_SERVICE_NAME
  }

  depends_on = [google_cloud_run_service.vault ]
}

output "url" {
  value = jsondecode(data.http.service_status.response_body).status.url
  description = "This is the url GCP assigns to access the Vault Cloud Run instance."
}

output "vault_dns_records" {
  value = google_cloud_run_domain_mapping.vault.status[0].resource_records
  description = "The resource records required to configure this domain mapping. These records must be added to the domain's DNS configuration in order to serve the application via this domain mapping."
}


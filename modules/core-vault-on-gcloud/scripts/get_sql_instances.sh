#!/bin/bash

eval "$(jq -r '@sh "GCP_ACCESS_TOKEN=\(.GCP_ACCESS_TOKEN)"')"

projects=`curl --silent -H "Authorization: Bearer $GCP_ACCESS_TOKEN" -X GET https://cloudresourcemanager.googleapis.com/v1/projects | jq -r '.projects[].projectNumber'`

ret=''

for project in $projects; do
  instances=`curl --silent -H "Authorization: Bearer $GCP_ACCESS_TOKEN" -X GET https://sqladmin.googleapis.com/v1/projects/${project}/instances | jq -r '.items[].connectionName ' 2> /dev/null`  
  if [ ! -z $instances ]; then
    for instance in $instances; do
      if [ -z $ret ]; then
        ret=$instance
      else
        ret=${ret},${instance}
      fi
    done
  fi
done
jq -n --arg instances "$ret" '{"instances":$instances}'

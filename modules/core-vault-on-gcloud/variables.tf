# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# These parameters must be supplied.
# ---------------------------------------------------------------------------------------------------------------------

variable "VAULT_PROJECT_ID" {
  description = "This is the infrastructure project to deploy vault under - project id"
}

variable "VAULT_PROJECT_NUMBER" {
  description = "This is the infrastructure project to deploy vault under - project number"
}

variable "GCP_ACCESS_TOKEN" {
  description = "Short lived GCP access token.  Get using `gcloud auth print-access-token`"
}

variable "ORG_ID" {
  description = "Organization id in GCP"
}


# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# ---------------------------------------------------------------------------------------------------------------------

variable "VAULT_VERSION" {
  description = "This is the docker tagged version of Vault"
  default     = "1.0.0"
}

variable "VAULT_SERVICE_NAME" {
  description = "Name of Vault service as it appears in Cloud Run"
  type = string
  default = "vault"
}

variable "DOMAIN" {
  description = "This is the domain for which your GCP account is hosted under.  Supplied by CookieCutter template."
  type        = string
}

variable "VAULT_DOMAIN" {
  description = "This is the fully qualified domain to map the Vault Instance to."
  type        = string
}
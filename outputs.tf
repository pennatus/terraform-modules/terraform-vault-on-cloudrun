output "vault_project_id" {
  value = length(var.VAULT_PROJECT_ID) == 0 ? google_project.vault_project[0].id : var.VAULT_PROJECT_ID
  description = "This is the project id hosting the vault instance"
}

output "vault_project_number" {
  value = data.google_project.vault_project.number
  description = "This is the project id hosting the vault instance"
}

output "vault_url" {
  value = module.vault_on_gcloud_1.url
  description = "This is the url GCP assigns to access the Vault Cloud Run instance"
}

output "vault_dns_records" {
  value = module.vault_on_gcloud_1.vault_dns_records
  description = "The resource records required to configure this domain mapping. These records must be added to the domain's DNS configuration in order to serve the application via this domain mapping"
}

output "org_id" {
  value = data.google_organization.org.org_id
  description = "The id of the organization"
}
# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# These parameters must be supplied and are secretive
# ---------------------------------------------------------------------------------------------------------------------

variable "GCP_ACCESS_TOKEN" {
  description = "Short lived GCP access token.  Get using `gcloud auth print-access-token`"
  type        = string
}

variable "VAULT_PROJECT_ID" {
  description = "Project ID that will be hosting the Vault instance.  Leave empty to let this module create a project for you."
  type        = string
  default     = ""
}

variable "VAULT_PROJECT_NAME" {
  description = "Project name to assign project hosting Vault."
  type        = string
  default     = ""
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# These parameters can be specified to override defaults.
# ---------------------------------------------------------------------------------------------------------------------

variable "DOMAIN" {
  description = "This is the domain for which your GCP account is hosted under.  Supplied by CookieCutter template."
  type        = string
}

variable "VAULT_DOMAIN" {
  description = "This is the fully qualified domain to map the Vault Instance to."
  type        = string
}

variable "BILLING_NAME_GENERAL" {
  description = "Display name for a billing account"
  type = string
  default = "My Billing Account"
}